import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kuis/webview_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    // Wait for 3 seconds and then navigate to the main screen
    Future.delayed(Duration(seconds: 3)).then((_) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => WebViewScreen(),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    // Hide the status bar and navigation bar
    SystemChrome.setEnabledSystemUIOverlays([]);
    
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/splash_screen.jpg'),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
