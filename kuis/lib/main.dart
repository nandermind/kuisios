import 'package:flutter/material.dart';
import 'package:kuis/splash_screen.dart';
import 'package:webview_flutter/webview_flutter.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter WebView Demo',
      home: SplashScreen(),
    );
  }
}

